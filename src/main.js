import 'normalize.css'
import Vue from 'vue'
import store from './store'
import App from './App.vue'

const app = new Vue({
  store,
  el: '#app',
  render: h => h(App)
})
