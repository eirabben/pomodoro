import moment from 'moment'

const module = {
  state: {
    config: {
      sessionLength: 30,
      shortBreakLength: 300,
      longBreakLength: 1200
    },
    remaining: null
  },

  mutations: {
    START_TIMER(state, { duration }) {
      state.remaining = duration
    },

    STOP_TIMER(state) {
      state.remaining = null
    },

    TICK(state) {
      state.remaining--
    }
  },

  actions: {
    startSession({ commit, state }) {
      commit('START_TIMER', { duration: state.config.sessionLength })

      const counter = setInterval(() => {
        commit('TICK')

        if (state.remaining === 0) {
          clearInterval(counter)
          commit('STOP_TIMER')
        }
      }, 1000)
    },

    startShortBreak() {

    },

    startLongBreak() {

    }
  },

  getters: {
    timeRemaining(state) {
      const remaining = moment.duration(state.remaining, 'seconds')
      const minutes = remaining.minutes()
      const seconds = remaining.seconds()
      return `${minutes}:${seconds}`
    }
  }
}

export default module
